import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Semaphore;

public class ConnectionNode {

    private final String url;
    private final String username;
    private final String password;
    private final String driver;

    private final int id;
    private boolean available;
    private Connection connection;
    private boolean closed;
    private Semaphore semaphore;

    public ConnectionNode(int id, String url, String username, String password, String driver) {
        this.id = id;
        this.url = url;
        this.username = username;
        this.password = password;
        this.driver = driver;
        this.closed = false;
        this.available = true;
        this.semaphore = new Semaphore(1);
    }

    public void createConnection() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("CONNECTION-" + getId() + " CREATED");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public int getId() {
        return id;
    }

    public boolean getAvailable() {
        return available;
    }

    public void setAvailable(boolean available) throws InterruptedException {
        if (available) this.semaphore.release();
        else this.semaphore.acquire();

        this.available = available;
    }

    public void close() {
        try {
            setClosed(true);
            this.connection.close();
            System.out.println("CONNECTION-" + getId() + " CLOSED");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isClosed(){
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
