import java.util.*;
import java.util.concurrent.Semaphore;

public class ConnectionPool {
    private final String url;
    private final String username;
    private final String password;
    private final String driver;
    private static int counter = 0;
    private static final int INITIAL_POOL_SIZE = 20;
    private static final int MAX_POOL_SIZE = 100;
    private static final List<ConnectionNode> pool = new ArrayList<>(MAX_POOL_SIZE);
    private Semaphore poolSemaphore;

    public ConnectionPool(String url, String username, String password, String driver) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driver = driver;
        this.poolSemaphore = new Semaphore(1);
    }

    public void createPool() throws InterruptedException {
        System.out.println("CONNECTION POOL CREATED");
        poolSemaphore.acquire();
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            ConnectionNode newConnection = new ConnectionNode(i, this.url, this.username, this.password, this.driver);
            newConnection.createConnection();
            pool.add(newConnection);
        }
        poolSemaphore.release();
    }

    public Optional<ConnectionNode> getConnectionInstance() throws InterruptedException {
        if (++counter % 200 == 0) checkForUnusedConnections();

        Optional<ConnectionNode> connection = pool.stream().filter(ConnectionNode::getAvailable).findFirst();

        if (connection.isPresent()) {
            return Optional.of(acquireConnection(connection.get()));
        } else {
            if (pool.size() < MAX_POOL_SIZE) {
                return Optional.of(acquireConnection(addNewConnectionToThePool()));
            } else {
                System.out.println("Sorry, Connection Pool is FULL!");
                return Optional.empty();
            }
        }
    }

    private void checkForUnusedConnections() throws InterruptedException {
        poolSemaphore.acquire();
        Iterator<ConnectionNode> iterator = pool.iterator();

        while (iterator.hasNext()) {
            ConnectionNode node = iterator.next();
            if (node.getConnection() != null) {
                boolean isClosed = node.isClosed();
                if (isClosed) {
                    System.out.println("CONNECTION-" + node.getId() + " REMOVED");
                    iterator.remove();
                }
            }
        }

        poolSemaphore.release();
    }

    private ConnectionNode acquireConnection(ConnectionNode connection) {
        try {
            connection.setAvailable(false);
            System.out.println("CONNECTION-" + connection.getId() + " IS BUSY NOW");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private ConnectionNode addNewConnectionToThePool() throws InterruptedException {
        poolSemaphore.acquire();
        ConnectionNode conn = new ConnectionNode(pool.size(), this.url, this.username, this.password, this.driver);
        conn.createConnection();

        pool.add(conn);
        poolSemaphore.release();
        return conn;
    }
}
