import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ConnectionPoolTests {

    private final static String URl = "jdbc:postgresql://localhost:5432/client-server";
    private final static String USER = "daniel";
    private final static String PASSWORD = "passWord";
    private final static String DRIVER = "org.postgresql.Driver";
    ConnectionPool pool;

    @BeforeEach
    void setUp() {
        pool = new ConnectionPool(URl, USER, PASSWORD, DRIVER);

    }
}
